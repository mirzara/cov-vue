import axios from 'axios'
import config from '../config'

import { notify } from "@kyvg/vue3-notification";

const instance = axios.create({
    baseURL: config.api_base_url
});

instance.interceptors.response.use((response) => response, (error) => {    
    notify({
        type:'error',
        title: 'Message',
        text: 'There is a problem retrieving data!'
      })

    throw error;
  });

export default instance