import instance from '../setup/axiossetup'

export default{
    getSummary(){
         return instance.get("/summary")
    },
    getCountries(){
        return instance.get("/countries")
    },
    liveByCountryAndStatus(countrySlug, status){
        return instance.get(`/live/country/${countrySlug}/status/${status}`)
    }
}