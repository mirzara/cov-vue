import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/summary',
    name: 'Summary',
    component: () => import('../views/Summary.vue')
  },
  {
    path: '/recovered/:country?',
    name: 'Recovered',
    component: () => import('../views/CountryCase.vue'),
    props: {
      default: true,
      message: 'Recovered',
      status: 'recovered',
      columns:[
        {fieldName:"Date", title:"Date", type:"Date"},
        {fieldName:"Country", title:"Country"},
        {fieldName:"Recovered", title:"Recovered"}
      ],
    }
  },
  {
    path: '/deaths/:country?',
    name: 'Deaths',
    component: () => import('../views/CountryCase.vue'),
    props: {
      default: true,
      message: 'Deaths ',
      status: 'deaths',
      columns:[
        {fieldName:"Date", title:"Date", type:"Date"},
        {fieldName:"Country", title:"Country"},
        {fieldName:"Deaths", title:"Deaths"}
      ],
    }
  },
  {
    path: '/confirmed/:country?',
    name: 'Confirmed',
    component: () => import('../views/CountryCase.vue'),
    props: {
      default: true,
      message: 'Confirmed / Active ',
      status: 'deaths',
      columns:[
        {fieldName:"Date", title:"Date", type:"Date"},
        {fieldName:"Country", title:"Country"},
        {fieldName:"Active", title:"Active"},
        {fieldName:"Confirmed", title:"Confirmed"}
      ],
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
